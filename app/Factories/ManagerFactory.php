<?php

namespace App\Factories;

use App\Utilities\CurlUtility;
use App\Repositories\GoogleSearchRepository;
use App\Singleton\RedisAdapter;
use App\Repositories\RedisCacheRepository;
use App\Managers\WebSearchManager;
use App\Config;

class ManagerFactory
{
    public static function createWebSearch(){

        $config = Config::getConfig();

        if(!($config['googleSearchEndPoint'] && $config['googleSearchEngineId'] && $config['googleSearchApiKey'] && $config['googleSearchItemsPerPage'] && $config['googleSearchNumberOfResults'])){
            throw new \Exception('Missing Google search configuration values', 4011);
        }
        $curlUtility            = new CurlUtility();
        $googleSearch           = new GoogleSearchRepository($curlUtility, $config['googleSearchEndPoint'], $config['googleSearchEngineId'], $config['googleSearchApiKey'], $config['googleSearchItemsPerPage'], $config['googleSearchNumberOfResults']);

        if(!($config['redisHost'] && $config['redisPort'] && $config['cacheNameSpace'] && $config['cacheLifeTime'])){
            throw new \Exception('Missing redis configuration values', 4012);
        }
        $redisAdapter           = RedisAdapter::connect($config['redisHost'], $config['redisPort'], $config['redisPassword'], $config['cacheNameSpace']);

        $cache                  = new RedisCacheRepository($redisAdapter);

        return new WebSearchManager($googleSearch, $cache);

    }
}