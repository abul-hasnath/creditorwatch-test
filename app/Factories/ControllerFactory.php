<?php

namespace App\Factories;


use App\Controllers\WebSearchController;

class ControllerFactory
{
    public static function createWebSearch(){
        $searchManager = ManagerFactory::createWebSearch();
        return new WebSearchController($searchManager);
    }

}