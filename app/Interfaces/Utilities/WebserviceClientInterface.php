<?php

namespace App\Interfaces\Utilities;


interface WebserviceClientInterface
{
    /**
     * @param string $url
     * @return mixed
     */
    public function get(string $url);

}