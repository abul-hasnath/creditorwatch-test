<?php


namespace App\Interfaces\Repositories;


interface CacheRepositoryInterface
{
    /**
     * @param string $key
     * @param int $seconds
     * @param string $data
     */
    public function saveData(string $key, int $seconds, string $data);

    /**
     * @param string $key
     * @return null|string
     */
    public function getData(string $key): ?string;

}