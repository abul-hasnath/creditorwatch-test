<?php

namespace App\Interfaces\Repositories;


interface WebSearchRepositoryInterface
{
    /**
     * @param string $keyword
     * @return array
     */
    public function getAll(string $keyword):array ;

}