<?php

namespace App\Interfaces\Managers;


interface WebSearchManagerInterface
{
    /**
     * @param string $keyword
     * @param string $domainName
     * @return int
     */
    public function findDomainNameInWebSearchResult(string $keyword, string $domainName): int;

}