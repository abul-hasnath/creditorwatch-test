<?php

namespace App\DataModels;


class SearchResultItem implements \JsonSerializable
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $link;
    /**
     * @var string
     */
    private $displayLink;

    function __construct()
    {
    }

    public function setTitle(string $title){
        $this->title = $title;
    }
    public function getTitle(): string {
        return $this->title;
    }
    public function setLink(string $link){
        $this->link = $link;
    }
    public function getLink(): string {
        return $this->link;
    }
    public function setDisplayLink(string $displayLink){
        $this->displayLink = $displayLink;
    }
    public function getDisplayLink(): string {
        return $this->displayLink;
    }
    public function isMatch(string $domainName){

        return strtolower($domainName) == strtolower($this->displayLink);
    }

    public function jsonSerialize()
    {
        return[
            'title' => $this->title,
            'link' => $this->link,
            'displayLink' => $this->displayLink
        ];
    }

}