<?php

namespace App\Utilities;

use App\Interfaces\Utilities\WebserviceClientInterface;

class CurlUtility implements WebserviceClientInterface
{
    function __construct()
    {
    }

    /**
     * @param string $url
     * @return mixed
     * @throws \Exception
     */
    public function get(string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (strtolower(substr($url, 0, 5)) == 'https') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        $info = curl_exec($ch);
        $status = curl_getinfo($ch);
        curl_close($ch);

        if($status['http_code'] == 200 || $status['http_code'] == 202){
            return $info;
        }
        else{
            throw new \Exception('An error occurred while searching the web.', 1011);
        }
    }
}