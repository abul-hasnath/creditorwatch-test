<?php

namespace App\Repositories;
use App\Interfaces\Repositories\CacheRepositoryInterface;
use \Predis\Client;


class RedisCacheRepository implements CacheRepositoryInterface
{
    private $cache;

    function __construct(Client $cache){
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @param int $seconds
     * @param string $data
     */
    public function saveData(string $key, int $seconds, string $data)
    {
        $this->cache->setex($key, $seconds, $data);

    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getData(string $key): ?string
    {
        if(!$this->cache->exists($key)){
            return null;
        }
        $value = $this->cache->get($key);

        if(empty($value)){
            return null;
        }
        return $value;

    }

}