<?php

namespace App\Repositories;

use App\Interfaces\Repositories\WebSearchRepositoryInterface;
use App\Interfaces\Utilities\WebserviceClientInterface;
use App\DataModels\SearchResultItem;


class GoogleSearchRepository implements WebSearchRepositoryInterface
{
    private $webSearchClient;
    private $endPoint;
    private $searchEngineId;
    private $itemsPerPage;
    private $searchEngineApiKey;
    private $numberOfItemsToProcess;

    private $startIndex;
    private $items;
    private $numberOfItemsToFetch;
    private $isNextPageAvailable;
    private $totalResults;

    /**
     * GoogleSearchRepository constructor.
     * @param WebserviceClientInterface $webSearchClient
     * @param string $endPoint
     * @param string $searchEngineId
     * @param string $searchEngineApiKey
     * @param int $itemsPerPage
     * @param int $numberOfItemsToProcess
     */
    function __construct(WebserviceClientInterface $webSearchClient, string $endPoint, string $searchEngineId, string $searchEngineApiKey, int $itemsPerPage, int $numberOfItemsToProcess)
    {
        $this->webSearchClient = $webSearchClient;
        $this->endPoint = $endPoint;
        $this->searchEngineId = $searchEngineId;
        $this->searchEngineApiKey = $searchEngineApiKey;
        $this->itemsPerPage = $itemsPerPage;
        $this->numberOfItemsToProcess = $numberOfItemsToProcess;

        $this->startIndex = 1;
        $this->numberOfItemsToFetch = 0;
        $this->items = [];
        $this->isNextPageAvailable = false;
        $this->totalResults = 0;

    }

    /**
     * @param string $keyword
     * @return string
     */
    private function search(string $keyword)
    {
        $url = $this->endPoint . '?' . 'key=' . $this->searchEngineApiKey . '&cx=' . $this->searchEngineId . '&num=' . $this->itemsPerPage . '&start=' . $this->startIndex . '&q=' . urlencode($keyword);

        $result = json_decode($this->webSearchClient->get($url));

        return $result;
    }

    /**
     * @param string $keyword
     * @throws \Exception
     */
    private function process(string $keyword)
    {
        if(count($this->items) >= $this->numberOfItemsToProcess){
            $this->isNextPageAvailable = false;
            return;
        }

        $result = $this->search($keyword);



        if (isset($result->error)) {

            $this->isNextPageAvailable = false;
            throw new \Exception('Search engine returned an error:' . $result->error->message, 2011);
        }

        $numberOfItemsReturned = intval($result->queries->request[0]->totalResults);
        if($numberOfItemsReturned == 0){
            return;
        }

        if($result->queries->request[0]->startIndex == 1){

            $this->totalResults = $numberOfItemsReturned;

            if($numberOfItemsReturned > $this->numberOfItemsToProcess){
                $this->numberOfItemsToFetch = $this->numberOfItemsToProcess;
            }elseif($numberOfItemsReturned < $this->numberOfItemsToProcess){
                $this->numberOfItemsToFetch = $numberOfItemsReturned;
            }
        }

        if(isset($result->queries->nextPage[0])){
            $nextPageStartIndex = intval($result->queries->nextPage[0]->startIndex);
            $nextPageResultCount = intval($result->queries->nextPage[0]->count);

            if($nextPageStartIndex > 0){
                $this->startIndex = $nextPageStartIndex;
                $this->isNextPageAvailable = true;
                $this->itemsPerPage = $nextPageResultCount;
            }

        }

        if(!empty($result->items)){
            foreach($result->items as $item){
                $searchItem = new SearchResultItem();
                $searchItem->setTitle($item->title);
                $searchItem->setLink($item->link);
                $searchItem->setDisplayLink($item->displayLink);
                $this->items[] = $searchItem;
            }
        }

    }

    /**
     * @param string $keyword
     * @return SearchResultItem[] array
     */
    public function getAll(string $keyword):array {
        $this->process($keyword);

        while($this->isNextPageAvailable){
            $this->process($keyword);
        }
        return $this->items;

    }


    }



