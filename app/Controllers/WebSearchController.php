<?php

namespace App\Controllers;

use App\Interfaces\Managers\WebSearchManagerInterface;

class WebSearchController extends BaseController
{
    private $webSearchManager;

    /**
     * WebSearchController constructor.
     * @param WebSearchManagerInterface $webSearchManager
     */
    function __construct(WebSearchManagerInterface $webSearchManager)
    {
        parent::__construct();
        $this->webSearchManager = $webSearchManager;
    }

    public function index(){
        $templateData = [];

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $keyword = trim($_POST['keyword']);
            $url = trim($_POST['url']);

            $count = $this->webSearchManager->findDomainNameInWebSearchResult($keyword, $url);

            $templateData['form']['keyword'] = $keyword;
            $templateData['form']['url'] = $url;
            $templateData['result']['count'] = $count;

        }
        echo $this->templateEngine->render('index.html', $templateData);
    }



}