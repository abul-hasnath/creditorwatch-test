<?php

namespace App\Controllers;

class BaseController
{
    protected $templateEngine;

    function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(ABSPATH . 'app/Views');
        $this->templateEngine = new \Twig_Environment($loader, [
            'cache' => ABSPATH . 'app/Views/Cache',
        ]);

    }

}