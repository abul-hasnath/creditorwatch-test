<?php
/**
 * Created by PhpStorm.
 * User: shojib
 * Date: 2/10/19
 * Time: 12:02 PM
 */

namespace App\Singleton;


class RedisAdapter
{
    private static $client;

    protected function __construct(){

    }

    private function __clone(){

    }

    /**
     * @param string $host
     * @param string $port
     * @param string $password
     * @param string $prefix
     * @return bool|\Predis\Client
     */
    public static function connect(string $host, string $port, string $password, string $prefix){

        try{
            if(!self::$client){
                $options = [
                    'scheme'    => 'tcp',
                    'host'      => $host,
                    'port'      => $port,
                    'prefix'    => $prefix];

                if($password){

                    $options['password'] = $password;
                }
                self::$client = new \Predis\Client($options);
            }
            return self::$client;

        }catch (\Exception $e){
            echo $e->getMessage();
            return false;
        }
    }


}