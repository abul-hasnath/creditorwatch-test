<?php

namespace App;


class Config
{
    protected function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getConfig(){

        return [
            'googleSearchEndPoint' => getenv('GOOGLE_SEARCH_ENDPOINT'),
            'googleSearchItemsPerPage' => getenv('ITEMS_PER_PAGE'),
            'googleSearchNumberOfResults' => getenv('PROCESS_SEARCH_RESULTS'),
            'googleSearchApiKey' => getenv('CSE_API_KEY'),
            'googleSearchEngineId' => getenv('SEARCH_ENGINE_ID'),
            'redisHost' => getenv('REDIS_HOST'),
            'redisPort' => getenv('REDIS_PORT'),
            'redisPassword' => getenv('REDIS_PASSWORD'),
            'cacheNameSpace' => getenv('CACHE_NAMESPACE'),
            'cacheLifeTime' => getenv('CACHE_LIFETIME')
        ];
    }
}