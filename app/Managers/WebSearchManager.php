<?php

namespace App\Managers;

use App\DataModels\SearchResultItem;
use App\Interfaces\Managers\WebSearchManagerInterface;
use App\Interfaces\Repositories\CacheRepositoryInterface;
use App\Interfaces\Repositories\WebSearchRepositoryInterface;
use App\Config;

class WebSearchManager implements WebSearchManagerInterface
{
    protected $webSearchRepository;
    protected $cacheRepository;

    /**
     * WebSearchManager constructor.
     * @param WebSearchRepositoryInterface $webSearchRepository
     * @param CacheRepositoryInterface $cacheRepository
     */
    function __construct(WebSearchRepositoryInterface $webSearchRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->webSearchRepository = $webSearchRepository;
        $this->cacheRepository = $cacheRepository;
    }

    /**
     * @param string $keyword
     * @return SearchResultItem[] array
     */
    private function getSearchResults(string $keyword): array {

        $config = Config::getConfig();
        $cacheKey = md5($keyword);
        $data = [];

        if($cachedData = $this->cacheRepository->getData($cacheKey)){
            //refresh cache
            $this->cacheRepository->saveData($cacheKey, $config['cacheLifeTime'], $cachedData);

            $dataObjects = json_decode($cachedData);

            foreach ($dataObjects as $row){
                $searchResultItem = new SearchResultItem();
                $searchResultItem->setTitle($row->title);
                $searchResultItem->setLink($row->link);
                $searchResultItem->setDisplayLink($row->displayLink);
                $data[] = $searchResultItem;
            }

        }else{

            $data = $this->webSearchRepository->getAll($keyword);
            if(!empty($data)){
                $this->cacheRepository->saveData($cacheKey, $config['cacheLifeTime'], json_encode($data));
            }
        }
        return $data;

    }

    /**
     * @param string $keyword
     * @param string $domainName
     * @return int
     */
    public function findDomainNameInWebSearchResult(string $keyword, string $domainName): int{
        $data = $this->getSearchResults($keyword);
        $domainName = trim($domainName);
        $count = 0;
        if(!empty($data)){
            foreach ($data as $item){
                if($item->isMatch($domainName)){
                    $count++;
                }
            }
        }
        return $count;
    }


}