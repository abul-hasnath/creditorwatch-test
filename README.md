# README #

This is a simple to find out how many time a website appears in Google's keyword search. 
This tool uses Google's custom search JSON API; so before running the tool we need to create a custom search engine and obtain search engine Id and API key.

### What we need ###

* PHP v 7.1+
* Redis

### Dependencies ###

* predis
* phpdotenv
* twig

### Setup ###

* Run the following commands in command prompt:
```
		$ git clone https://abul-hasnath@bitbucket.org/abul-hasnath/creditorwatch-test.git
```
```
		$ cd /project-directory
```
```
		$ composer install
```
```
		$ composer dump-autoload -o
```
* Make a copy of example.env and rename it to .env
* Set SEARCH_ENGINE_ID and CSE_API_KEY values. Set redis configuration values according to your setup
* Set write permission in app/Views/Cache

### Architecture ###

* This application uses Factory pattern to create some application control objects, namely Controllers and Managers. 
* Dependency Injection is used in classes under Controllers, Managers, Repositories, Utilities.
* Data storage uses Singleton pattern (app/Singleton/RedisAdapter)
* Class definations under Managers, Repositories, Utilities use interface based abstraction layer for dependencies.
* This application is far from perfact. I wanted to implement route manager and reflection based smart loading of classes; then decided to let it pass for now.
